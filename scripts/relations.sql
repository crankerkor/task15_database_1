-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema relations
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema relations
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `relations` DEFAULT CHARACTER SET utf8mb4 ;
USE `relations` ;

-- -----------------------------------------------------
-- Table `relations`.`family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `relations`.`family_tree` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `birth_date` VARCHAR(45) NULL DEFAULT NULL,
  `death_date` VARCHAR(45) NULL DEFAULT NULL,
  `credit_card` VARCHAR(45) NULL DEFAULT NULL,
  `family_tree_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_family_tree_family_tree1_idx` (`family_tree_id` ASC),
  CONSTRAINT `fk_family_tree_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `relations`.`family_tree` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `relations`.`family_value`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `relations`.`family_value` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `approximate_price` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `max_price` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `min_price` INT(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  `family_valuecol` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `relations`.`family_tree_has_family_value`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `relations`.`family_tree_has_family_value` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `family_tree_id` INT(11) NOT NULL,
  `family_value_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_family_tree_has_family_value_family_value1_idx` (`family_value_id` ASC),
  INDEX `fk_family_tree_has_family_value_family_tree1_idx` (`family_tree_id` ASC),
  CONSTRAINT `fk_family_tree_has_family_value_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `relations`.`family_tree` (`id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_family_tree_has_family_value_family_value1`
    FOREIGN KEY (`family_value_id`)
    REFERENCES `relations`.`family_value` (`id`)
    ON DELETE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `relations`.`sex`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `relations`.`sex` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sex` VARCHAR(10) NOT NULL,
  `family_tree_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `family_tree_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_sex_family_tree1_idx` (`family_tree_id` ASC),
  CONSTRAINT `fk_sex_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `relations`.`family_tree` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `relations`.`relative`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `relations`.`relative` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `name_surname` VARCHAR(90) NULL DEFAULT NULL,
  `birth_date` DATE NULL DEFAULT NULL,
  `death_date` DATE NULL DEFAULT NULL,
  `birth_place` VARCHAR(45) NULL DEFAULT NULL,
  `death_place` VARCHAR(45) NULL DEFAULT NULL,
  `marriage_date` DATE NULL DEFAULT NULL,
  `family_tree_id` INT(11) NOT NULL,
  `sex_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `family_tree_id`, `sex_id`),
  UNIQUE INDEX `idrelatives_UNIQUE` (`id` ASC),
  INDEX `fk_relatives_family_tree_idx` (`family_tree_id` ASC),
  INDEX `fk_relative_sex1_idx` (`sex_id` ASC),
  CONSTRAINT `fk_relative_sex1`
    FOREIGN KEY (`sex_id`)
    REFERENCES `relations`.`sex` (`id`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_relatives_family_tree`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `relations`.`family_tree` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
